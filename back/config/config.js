require("dotenv").config();
module.exports = {
    table_detalle: 'detalle',
    table_login: 'login',
    aws_remote_config: {
      region: 'us-east-1',
      endpoint: 'arn:aws:dynamodb:us-east-1:938919343525'
    },
    aws_local_config: {
      accessKeyId: process.env.ACCESSKEY_ID,
      secretAccessKey: process.env.SECRET_ACCESS_KEY,
      region: 'us-east-1',
    }
};
