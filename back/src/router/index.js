import express from "express";
import detalle from "../controllers/detalle";
import login from '../controllers/login';

const router = express.Router();

router.post('/detalle', function(req,res){
  detalle.post(req,res,req.body);
});

router.get('/detalle', function(req,res){
  detalle.get(req,res,req.body);
});

router.delete('/detalle', function(req,res){
  detalle.delete(req,res,req.body);
});

router.get('/login', function(req,res){
  login.get(req,res,req.body);
});

router.put('/login', function(req,res){
  login.buscar(req,res,req.body);
});

router.post('/login', function(req,res){
  login.post(req,res,req.body);
});

module.exports = router;
