import index from "../models";
const query = {};

query.get = (req, res) => {
  index.get(req, res, req.query);
};

query.post = (req, res) => {
  index.post(req, res, req.body);
};

query.delete = (req, res) => {
  index.delete(req,res,req.body);
};

module.exports = query;
