import index from "../models";
const query = {};

query.get = (req, res) => {
  index.getLogin(req, res, req.query);
};

query.buscar = (req, res) => {
  index.buscarLogin(req, res, req.query);
};

query.post = (req, res) => {
  index.guardarLogin(req, res, req.query);
};

module.exports = query;