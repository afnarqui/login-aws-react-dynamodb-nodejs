require("dotenv").config();
import config from "../../config/config";
import object from '../../config/detalle';
import AWS from "aws-sdk";
const Uuid = require('uuid/v1');
const query = {};

query.get = (request, res, params) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const param = {
    TableName: config.table_detalle
  };
  docClient.scan(param, function(err, data) {
    if (err) {
      res.send({
        success: false,
        message: "Error: Servidor"
      });
    } else {
      const { Items } = data;

      res.send({
        success: true,
        message: "cargar detalle",
        detalle: Items
      });
    }
  });
};

query.delete = (request, res, param) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const params = {
    TableName: config.table_detalle
  };
  docClient.delete(params, function(err, data) {
    if (err) {
      res.send({
        success: false,
        message: "error detalle",
        detalle: err
      });
    } else {
      res.send({
        success: true,
        message: "eliminar detalle",
        detalle: data
      });
    }
  });
};

query.post = (request, res, param) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();

  let response;
  let cantidad = object.length;
  for (var i = 0; i < object.length; i++) {
    const params = {
      TableName: config.table_detalle,
      Item: object[i]
    };

    docClient.put(params, function(err, data) {
      if (err) {
        response = {
          success: false,
          message: "error detalle",
          detalle: err
        };
        if(i == cantidad){
          return res.send(response);
        }
      } else {
        response = {
          success: true,
          message: "cargo datos detalle",
          detalle: data
        };
        if(i == cantidad){
          return  res.send(response);
        }
      }
    });
  }
  
};

query.getLogin = (request, res, params) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const param = {
    TableName: config.table_login
  };
  docClient.scan(param, function(err, data) {
    if (err) {
      res.send({
        success: false,
        message: "Error: Servidor"
      });
    } else {
      const { Items } = data;
      res.send({
        success: true,
        message: "cargar login",
        detalle: Items
      });
    }
  });
};

query.buscarLogin = (request, res, params) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const param = {
    TableName: config.table_login
  };
  docClient.scan(param, function(err, data) {
    if (err) {
      res.send({
        success: false,
        message: "Error: Servidor"
      });
    } else {
      let response;
      let cantidad = data.Items.length;
      for (var i = 0; i < data.Items.length; i++) {
        if(data.Items[i].clave == params.password && data.Items[i].correo == params.correo) {
          response =
          {
            success: true,
            message: "Usuario existente",
            detalle: data.Items[i].correo + data.Items[i].clave + data.Items[i].id
          };
        }
        
        if(i + 1 == cantidad){
          if(response===undefined){
            return res.send({success: false,
              message: "Usuario no encontrado",
              detalle: ''});
          }else{
            return res.send(response);
          }
        }
      }
    }
  });
};

query.guardarLogin = (request, res, params) => {
  AWS.config.update(config.aws_local_config);
  const docClient = new AWS.DynamoDB.DocumentClient();
    let response;
    let existeUsuario = 0;
    const table = {
      TableName: config.table_login
    };
    docClient.scan(table, function(err, data) {
      if (err) {
        existeUsuario = 0;
      } else {
        let cantidad = data.Items.length;
        for (var i = 0; i < data.Items.length; i++) {
          if(data.Items[i].clave == params.password && data.Items[i].correo == params.correo) {
            existeUsuario = 1;
          }
          if(i + 1== cantidad){
            if(existeUsuario==1){
              response = {
                success: false,
                message: "el usuario ya existe verifique...",
                detalle: ""
              };
                return  res.send(response);
            }
            let data = {
              clave: params.password,
              correo: params.correo,
              id:Uuid(),
            }
            const table = {
              TableName: config.table_login,
              Item: data
            };
            
            docClient.put(table, function(err, data) {
              if (err) {
                response = {
                  success: false,
                  message: "error login",
                  detalle: err
                };
                  return res.send(response);
              } else {
                response = {
                  success: true,
                  message: "guardo data login",
                  detalle: data
                };
                  return  res.send(response);
              }
            });
         }
        }
      }
    });
};

module.exports = query;
