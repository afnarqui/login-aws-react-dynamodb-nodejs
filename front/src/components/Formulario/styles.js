import styled from 'styled-components';

export const Divform = styled.div`
position: relative;
width: 100%;
margin-bottom: 20px;
`;

export const Label = styled.label`
display: inline-block;
font-size: 13px;
font-weight: 500;
color: #69708d;
margin-bottom: 5px;
width: 250px;
text-align: left;
margin-top: 4px;
padding-left: 10px;
`;

export const Input = styled.input`
width: 100%;
height: 38px;
border: 1px solid #d6dbf5;
padding-left: 20px;
padding-right: 20px;
box-sizing: border-box;
font-size: 2px;
font-weight: 500;
margin-left: 10px;
`;

export const Divinput = styled.div`
width:100%;
position: relative;
`;

export const Divbutton = styled.div`
display: "inline-block";
`;

export const Button = styled.button`
background-color: #f50f78;
color: #fff;
font-weight: "600";
text-transform: uppercase;
border: 0;
border-radius: 18px;
height: 36px;
padding-right: 30px;
line-height: 36px;
cursor: pointer;
transition: "background-color .2s ease-in-out,opacity .2s ease-in-out";
margin-top: 10px;
padding-left: 30px;
text-align: center;
`;