import React,  { Fragment, useState, useRef } from "react";
import { Button, Divform, Divbutton, Divinput, Label } from "./styles";
import { getLogin, postLogin } from '../../services/login';
import "@babel/polyfill";

export const Formulario = () => {
  const inputCorreo = useRef(null);
  const inputPasword = useRef(null);
  const [correo, useCorreo] = useState("");
  const [password, usePassword] = useState("");
  const [visible, useVisible] = useState(false);
  
  async function buscarLogin(datosCorreo,datosPassword) {
    useVisible(true)
    validarCampos(datosCorreo,datosPassword);
    const response = await getLogin(datosCorreo,datosPassword);
        if(response.data.success){
          window.sessionStorage.setItem('token', response.data.detalle);
          window.location.reload(true);
        }else {
          alert('el usuario no existe verifique');
          useVisible(false);
          return;
        }
        limpiarValores();
  }
 
  async function guardarLogin(datosCorreo,datosPassword) {
    useVisible(true)
    validarCampos(datosCorreo,datosPassword);
    const response = await postLogin(datosCorreo,datosPassword);
    if(response.data.success){
      alert('usuario registradoc con exito');
    }else {
      alert('el usuario no se guardo verifique');
    }
    limpiarValores();
  }

  const validarCampos = (datosCorreo,datosPassword) => {
    if((datosCorreo=='' || datosCorreo ===undefined) && datosPassword=='' || datosPassword ==undefined){
      alert('debe ingresar todos los datos');
      return;
    }
  }

  const handleSubmit = e => {
    e.preventDefault();
    buscarLogin(correo,password);
  };

  const limpiarValores = () => {
    useCorreo("");
    usePassword("");
    inputCorreo.current.value = "";
    inputPasword.current.value = "";
    useVisible(false);
  };

  const handleSubmitguardar = e => {
    e.preventDefault();
    let data = guardarLogin(correo,password)
    data.then((resp)=>{
      limpiarValores();
     },(error)=>{
       console.log('error',error);
       alert('se genero un error verifique');
       limpiarValores();
     });
  };
  
  return (
    <Fragment>
       {
        visible &&  <img style={{display:"block",margin:"auto"}} src="loader.gif" alt="Sin imagen" />
       }
      <Divform>
        <Label>Correo Electrónico:</Label>
        <Divinput>
          <input
            type="email"
            name="correo"
            ref={inputCorreo}
            onChange={e => useCorreo(e.target.value)}
          />
        </Divinput>
        <Label>Contraseña:</Label>
        <Divinput>
          <input
            type="password"
            name="password"
            ref={inputPasword}
            onChange={e => usePassword(e.target.value)}
          />
        </Divinput>
        <Divbutton>
          <Button type="button" onClick={handleSubmit} value="INGRESAR">
            INGRESAR
          </Button>
        </Divbutton>
        <Divbutton>
          <Button type="button" onClick={handleSubmitguardar} value="GUARDAR USUARIO">
            GUARDAR USUARIO
          </Button>
        </Divbutton>
      </Divform>
    </Fragment>
  );
};
