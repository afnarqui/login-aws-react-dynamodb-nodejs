import styled from 'styled-components';

export const Divnombrecompania = styled.button`
    background-color: rgb(255,255,255);
    border: 0;
    color: inherit;
    outline: 0;
    padding: 0px !important;
    margin-left: 35px !important;
    margin-right: 25px !important;
    margin-top: 12px !important;
`;

export const Hr = styled.hr`
  height: 6px;
  background-color: rgb(0, 0, 0);
  width: 150px;
  margin-left: 5px !important;
  margin-right: 5px !important;
  border-radius: 12px;
`;