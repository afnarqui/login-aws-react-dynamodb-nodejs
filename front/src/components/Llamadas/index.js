import React, { useState, useEffect } from "react";
import { Divnombrecompania, Hr } from "./style";
import { useDataValue } from '../../services/useDataValue';

const Imagen = {
  height: "15px",
  width: "50px",
  margin: "0px",
  padding: "0px",
};

export const Llamadas = () => {
  const [valores, useValores] = useState([]);

  useEffect(() => {
    const datos =  useDataValue();
   
    useValores(datos);
  }, []);

  return (
    <div>
      <div>
        <div>
          {valores.map(item => {
              let colorNumeros = item.colorNumero;
              let colorLinea = item.colorLinea;
            return (
              <Divnombrecompania>
                <p
                  style={{
                    color: colorNumeros,
                    marginButton: "0px",
                    marginTop: "0px",
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: "18px"
                  }}>
                  {item.numero}
                </p>
                <p
                  style={{
                    color: "rgb(196, 197, 197)",
                    marginButton: "0px",
                    marginTop: "0px",
                    textAlign: "center",
                    fontWeight: "bold",
                    fontSize: "14px"
                  }}>
                  {item.descripcion}
                </p>
                <img style={Imagen} src={item.img} alt="Sin imagen" />
               
                <p
              style={{
                backgroundColor: colorLinea,
                marginButton: "0px",
                textAlign: "center",
                borderRadius: "12px !important"
              }}>
              <hr style={{
                height: "6px",
                backgroundColor:colorLinea,
                width: "150px",
                marginLeft: "10px !important",
                marginRight: "10px !important",
                borderRadius: "4px !important"
              }}/>
            </p>
            
              </Divnombrecompania>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Llamadas;
