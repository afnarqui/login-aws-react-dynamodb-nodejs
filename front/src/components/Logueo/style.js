import styled from 'styled-components';

export const Fondo = styled.div`
  background: #504dd4 url('login.svg');
  width: 100vw;
  height: 100vh;
  left: 40%;
  position: fixed;
  top: 0;
  background-color: linear-gradient(159deg,#8a55e9,#1244be);
  z-index: 10;
  border-width: 200vh 0 0 120vw;
  border-color: transparent transparent transparent #fff;
  border-style: dashed;
`;

export const Wrapper = styled.div`
    position: absolute;
    z-index: "12";
    min-width: 320px;
    max-width: 320px;
    box-sizing: border-box;
    height: 470px;
    left: 40px;
    top: calc(50vh - 240px);
        `;
  
  export const Avatar = styled.div`
    margin-top: 20px;
    width: 120px;
    height: 120px;
    border: 2px solid #d6dbf5;
    margin-bottom: 40px;
    background-image: url(https://s3.amazonaws.com/gc-general/gc-robot.svg);
    background-color: #fff;
    background-position: 50%;
    background-repeat: no-repeat;
    background-size: 90px;
    `;
  
  export const Divform = styled.div`
    position: relative;
    width: 100%;
    margin-bottom: 20px;
  `;
  
  export const Label = styled.label`
    display: inline-block;
    font-size: 13px;
    font-weight: 500;
    color: #69708d;
    margin-bottom: 5px;
    width: 250px;
    text-align: left;
    margin-top: 4px;
    padding-left: 10px;
    `;
  
export const Input = styled.input`
    width: 100%;
    height: 38px;
    border: 1px solid #d6dbf5;
    padding-left: 20px;
    padding-right: 20px;
    box-sizing: border-box;
    font-size: 2px;
    font-weight: 500;
    margin-left: 10px;
    `;
  
  export const Divinput = styled.div`
    width:100%;
    position: relative;
    `;
  
  export const Formgeneral = styled.div`
    display: block;
    margin-top: 0em;
    margin-block-end: 1e;
    `;
  
  export const Divbutton = styled.div`
    display: "inline-block";
    `;
  
  export const Button = styled.button`
    background-color: #f50f78;
    color: #fff;
    font-weight: "600";
    text-transform: uppercase;
    border: 0;
    border-radius: 18px;
    height: 36px;
    padding-right: 30px;
    line-height: 36px;
    cursor: pointer;
    transition: "background-color .2s ease-in-out,opacity .2s ease-in-out";
    margin-top: 10px;
    padding-left: 30px;
    text-align: center;
    `;
  
  export const Footer = styled.div`
    position: absolute;
    bottom: 0px;
    left: 40px;
    z-index: "12";
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-content: center;
    align-items: "flex-star";
    `;
  
  export const Logofooter = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-content: center;
    align-items: center;
    font-size: 24px;
    font-weight: 500;
    color: #69708d;
    text-transform: uppercase;
    line-height: 18px;
    `;
  
  export const Spanfooter = styled.span`
    margin-right: 5px;
    display: inline-block;
    font-size: 34px;
    font-weight: 800;
    color: #009ada;
    text-transform: uppercase;
`;

export const Formularios = styled.form`
    position: relative;
    width: 100%;
    margin-bottom: 20px;
  `;