import React from "react";
import {
  Fondo,
  Wrapper,
  Formgeneral,
  Avatar,
  Footer,
  Logofooter,
  Spanfooter
} from "./style";
import { Formulario } from "../Formulario";

const Logueo = () => {
  return (
    <div>
      <Wrapper>
        <div>
          <div>
            <img src="logo.svg" alt="Sin imagen" />
          </div>
          <div>
            <Formgeneral>
              <Avatar />
              <Formulario />
            </Formgeneral>
          </div>
        </div>
        
      </Wrapper>
      <Fondo></Fondo>
      <Footer>
        <Logofooter>
          <Spanfooter>OMNI</Spanfooter>
          1.0
        </Logofooter>
        <p>
          Un servicio de
          <span>Gocloud</span>
        </p>
      </Footer>
    </div>
  );
};

export default Logueo;
