import styled from 'styled-components';

export const ImagenLeft = styled.img`
    height: 34px;
    width: 34px;
    float: left;
`;

export const ImagenRight = styled.img`
    height: 34px;
    width: 34px;
    float: right;
`;

export const Div = styled.button`
    background-color: rgb(245, 245, 245);
    border: 0;
    color: inherit;
    outline: 0;
    padding: 0;
    margin:0;
    width:100%;
`;

export const Divcompania = styled.button`
    background-color: rgb(255, 255, 255);
    border: 0;
    color: inherit;
    outline: 0;
    margin-left:25px;
    width:100%;
`;

export const Divnombrecompania = styled.button`
    background-color: rgb(0, 198, 255);
    border: 0;
    color: inherit;
    outline: 0;
    width:95%;
    height: 34px;
    padding-top:0px !important;
    margin-left: 35px !important;
    margin-right: 29px !important; 
    border-radius: 8px;
`;

export const Li = styled.li`
    color: white;
    text-align: center;
    padding-top:10px;
`;

export const Lilist = styled.li`
    border: 0;
    color: gray;
    font-size:20px;
    margin-bottom: 10px;
    text-align: center;
`;

export const DivtituloInterno = styled.div`
  background-color:rgb(180, 178, 242);
  
`;


