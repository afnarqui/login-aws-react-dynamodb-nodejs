import React, { useState, useEffect } from "react";
import { Div, Divcompania, Divnombrecompania, Li, Lilist, DivtituloInterno } from "./style";
import Llamadas from "../Llamadas";
import { getDetalle } from "../../services/login";
import "@babel/polyfill";

const ImagenLeft = {
  height: "55px",
  width: "150px",
  float: "left",
  marginButton: "20px",
  color: "rgb(255,255,255)"
};

const ImagenRight = {
  height: "35px",
  width: "45px",
  float: "right",
  backgroundColor: "rgb(158, 104, 213)",
  color: "white !important",
  borderRadius: "12px",
  marginTop: "16px",
  marginRight: "14px",
  marginButton: "0px"
};

const paginaLogin = () => {
  window.sessionStorage.removeItem("token");
  window.location.reload(true);
};

const Detalles = () => {
  const [valores, useValores] = useState([]);

  async function buscarDetalle() {
    const response = await getDetalle();
    console.log("buscar detalles", response);
    if (response.data.success) {
      useValores(response.data.detalle);
    } else {
      useValores([]);
    }
  }

  useEffect(() => {
    buscarDetalle();
  }, []);

  return (
    <div>
      <div>
        <div>
          <button
            style={{
              backgroundColor: "rgb(134, 109, 255)",
              width: "50%",
              height: "60px",
              border: "0",
              color: "inherit",
              outline: "0",
              padding: "0",
              
            }}>
            <img style={ImagenLeft} src="logo.svg" alt="Sin imagen" />
          </button>
          <button
            style={{
              backgroundColor: "rgb(134, 109, 255)",
              width: "50%",
              height: "60px",
              paddingTop: "10px !important",
              border: "0",
              color: "inherit",
              outline: "0",
              padding: "0",
              
            }}>
            <img
              onClick={() => paginaLogin()}
              style={ImagenRight}
              src="salir.svg"
              alt="Sin imagen"
            />
          </button>
        </div>
        <Div>
          <p
            style={{
              marginButton: "0px",
              textAlign: "center",
              paddingRight: "25px",
              float: "right",
              fontWeight: "bold",
              color: "#a8a9aa",
              fontSize: "14px",
              height: "10px"
            }}>
            OMNI MIA
          </p>{" "}
        </Div>
        <Divcompania>
          <p
            style={{
              color: "rgb(253, 98, 146)",
              marginButton: "0px",
              textAlign: "center",
              paddingRight: "25px",
              float: "left",
              fontWeight: "bold",
              fontSize: "14px"
            }}>
            Detalle de campaña
          </p>{" "}
        </Divcompania>
        <Divnombrecompania>
          <p
            style={{
              marginLeft: "10px",
              color: "rgb(255, 255, 255)",
              textAlign: "center",
              paddingLeft: "25px",
              float: "left",
              fontWeight: "bold",
              fontSize: "14px",
            }}>
            Nombre de la campaña
          </p>
          <p
            style={{
              color: "rgb(255, 255, 255)",
              textAlign: "center",
              paddingRight: "25px",
              float: "right",
              fontWeight: "bold",
              fontSize: "14px",
            }}>
            Progreso discado:73%
          </p>
          <p
            style={{
              color: "rgb(255, 255, 255)",
              textAlign: "center",
              paddingRight: "25px",
              float: "right",
              fontWeight: "bold",
              fontSize: "14px",
            }}>
            Duración promedio llamada: 24seg
          </p>

          <p
            style={{
              color: "rgb(255, 255, 255)",
              textAlign: "center",
              paddingRight: "25px",
              float: "right",
              fontWeight: "bold",
              fontSize: "14px",
            }}>
            Consumo total: 02.07 min
          </p>
        </Divnombrecompania>
        <Llamadas />
        <div style={{ width: "99%", marginBottom:"15px",paddingLeft:"30px"}}>
         
            <DivtituloInterno
              style={{
                width: "24%",
                float: "left",
                listStyle: "none",
                fontWeight: "bold",
                fontSize: "14px",
                paddingBottom: "8px",
                borderRadius: "8px 0px 0px 8px"
              }}>
              <Li>DATE</Li>
            </DivtituloInterno>
            <DivtituloInterno
              style={{
                width: "24%",
                float: "left",
                listStyle: "none",
                fontWeight: "bold",
                fontSize: "14px",
                paddingBottom: "8px"
              }}>
              <Li>ENTITY</Li>
            </DivtituloInterno>
            <DivtituloInterno
              style={{
                width: "24%",
                float: "left",
                listStyle: "none",
                fontWeight: "bold",
                fontSize: "14px",
                paddingBottom: "8px"
              }}>
              <Li>STATUS</Li>
            </DivtituloInterno>
            <DivtituloInterno
              style={{
                width: "24%",
                float: "left",
                listStyle: "none",
                fontWeight: "bold",
                fontSize: "14px",
                paddingBottom: "8px",
                borderRadius: "0px 8px 8px 0px"
              }}>
              <Li>STEP</Li>
            </DivtituloInterno>
         
        </div>
        <br></br>
        <div style={{ width: "100%" }}>
          {valores.map(item => {
            return (
              <ul>
                <div
                  style={{
                    width: "24%",
                    borderRadius: "8px",
                    float: "left",
                    listStyle: "none",
                    
                  }}>
                  <Lilist>{item.date}</Lilist>
                  
                </div>
                <div
                  style={{
                    width: "24%",
                    borderRadius: "8px",
                    float: "left",
                    listStyle: "none",
                    
                  }}>
                  <Lilist>{item.entity}</Lilist>
                  
                </div>
                <div
                  style={{
                    width: "24%",
                    borderRadius: "8px",
                    float: "left",
                    listStyle: "none",
                    
                  }}>
                  <Lilist>{item.status}</Lilist>
                  
                </div>
                <div
                  style={{
                    width: "24%",
                    borderRadius: "8px",
                    float: "left",
                    listStyle: "none",
                    
                  }}>
                  <Lilist>{item.step}</Lilist>
                  
                </div>
              </ul>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Detalles;
