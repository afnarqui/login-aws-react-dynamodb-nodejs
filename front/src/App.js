import React from "react";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import Context from "./Context";
import Logueo from "./components/Logueo";
import Detalles from "./components/Detalles";

export const App = () => {
  return (
    <div>
      <Context.Consumer>
        {({ isAuth }) =>
          isAuth ? (
            <HashRouter>
              <Switch>
                <Route path="/" component={Detalles} exact />
              </Switch>
            </HashRouter>
          ) : (
            <HashRouter>
              <Switch>
                <Redirect from='/login' to='/' />
                <Route path="/" component={Logueo} exact />
              </Switch>
            </HashRouter>
          )
        }
      </Context.Consumer>
    </div>
  );
};
