import axios from 'axios';

const baseUrl = `http://localhost:3002/`;
  
export async function getLogin(correo,password) {
  try {
    const response = await axios({
      url: `${baseUrl}login?correo=${correo}&password=${password}`,
      method: 'PUT',
    });

    return response;
  } catch (e) {}
}

export async function postLogin(correo,password) {
  try {
    const response = await axios({
      url: `${baseUrl}login?correo=${correo}&password=${password}`,
      method: 'POST',
    });

    return response;
  } catch (e) {}
}

export async function getDetalle() {
  try {
    const response = await axios({
      url: `${baseUrl}detalle`,
      method: 'GET',
    });

    return response;
  } catch (e) {}
}

