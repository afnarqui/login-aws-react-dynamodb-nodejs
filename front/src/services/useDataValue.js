
export const useDataValue = () => {
    const datos = [
      {
        colorNumero: "rgb(76, 156, 255)",
        numero: "36",
        descripcion: "Transferidos",
        colorLinea: "rgb(76, 156, 255)",
        img:'1.svg',
        id: 1
      },
      {
        colorNumero: "rgb(0, 190, 190)",
        numero: "3728",
        descripcion: "Iniciadas",
        colorLinea: "rgb(0, 190, 190)",
        img:'3.svg',
        id: 2
      },
      {
        colorNumero: "rgb(172, 66, 255)",
        numero: "11712",
        descripcion: "Gestionadas",
        colorLinea: "rgb(172, 66, 255)",
        img:'7.svg',
        id: 3
      },
      {
        colorNumero: "rgb(255, 193, 0)",
        numero: "7984",
        descripcion: "No iniciadas",
        colorLinea: "rgb(255, 193, 0)",
        img:'6.svg',
        id: 4
      },
      {
        colorNumero: "rgb(0, 198, 255)",
        numero: "11712",
        descripcion: "Números",
        colorLinea: "rgb(0, 198, 255)",
        img:'5.svg',
        id: 5
      },
      {
        colorNumero: "rgb(206, 19, 28)",
        numero: "10",
        descripcion: "Fallidos",
        colorLinea: "rgb(206, 19, 28)",
        img:'2.svg',
        id: 6
      },
      {
        colorNumero: "rgb(206, 19, 148)",
        numero: "14",
        descripcion: "Compromiso pago",
        colorLinea: "rgb(206, 19, 148)",
        img:'4.svg',
        id: 7
      }
    ];
     return datos;
 };
