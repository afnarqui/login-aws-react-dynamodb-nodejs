# login-aws-react-dynamodb-nodejs

[![N|Solid](https://res.cloudinary.com/drqk6qzo7/image/upload/v1574794263/inicial_iwq7xy.png)](https://res.cloudinary.com/drqk6qzo7/image/upload/v1574794263/inicial_iwq7xy.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## EC2 

[![Build Status](https://res.cloudinary.com/drqk6qzo7/image/upload/v1574807310/EC2_nlyrub.png)](https://res.cloudinary.com/drqk6qzo7/image/upload/v1574807310/EC2_nlyrub.png)

## Dynamodb

[![Build Status](https://res.cloudinary.com/drqk6qzo7/image/upload/v1574807310/dynamodb_gshvlo.png)](https://res.cloudinary.com/drqk6qzo7/image/upload/v1574807310/dynamodb_gshvlo.png)


Este es el proyecto que permite hacer un login y consultar los detalles. desarrollado en react con nodejs, dynamodb y aws

[Api rest](https://web.postman.co/collections/7654104-14d8c840-fa88-4c05-8abc-e1ced4bd5140?version=latest&workspace=00bc3c90-3900-498f-878f-23586df2ec7d) documentación de la api rest

## Uso

Requiere

[Node.js](https://nodejs.org/) v10+

[Docker](https://hub.docker.com/editions/community/docker-ce-desktop-windows)

[Git](https://git-scm.com/downloads)

[React](https://es.reactjs.org/)

[Dynamodb](https://aws.amazon.com/es/dynamodb/)

código abierto compartido en [public repository][afn]
GitLab.


## Forma de ejecutar el proyecto en desarrollo

````bash
git clone https://gitlab.com/afnarqui/login-aws-react-dynamodb-nodejs.git
cd login-aws-react-dynamodb-nodejs/back
npm install
cd front
npm run build
cd ..
npm run dev
http://localhost:3002
````

## Forma de trabajar con las ramas
# crear un issue

````bash
git branch 1-cu1-create-repositorio-back origin/1-cu1-create-repositorio-back
git checkout 1-cu1-create-repositorio-back
````

## Forma de ejecutar Dynomodb de forma local
````bash
java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb
aws dynamodb list-tables --endpoint-url http://localhost:8000
aws dynamodb create-table --cli-input-json file://./config/detalle.json --endpoint-url http://localhost:8000
aws dynamodb list-tables --endpoint-url http://localhost:8000
aws dynamodb scan --table-name detalle --endpoint-url http://localhost:8000
aws dynamodb delete-table --table-name detalle --endpoint-url http://localhost:8000
````

## Forma de ejecutar Dynomodb con docker
````bash 
docker pull amazon/dynamodb-local
docker run -i -t --volumes-from datos -v d:/developement:/d -p 8000:8000 --name bd amazon/dynamodb-local
docker exec -it bd  bash
````

## Forma de crear imagenes locales en docker y probarlas
````bash
cd front
npm run build
cd ..
cd back
docker build -t aws:v1 .
docker run -dit -p 8:80 aws:v1
http://localhost
````

## Forma de crear un registry en gitlab y subir las imagenes de docker
````bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/afnarqui/login-aws-react-dynamodb-nodejs .
docker push registry.gitlab.com/afnarqui/login-aws-react-dynamodb-nodejs
````

## Forma de crear imagenes en aws y probarlas
````bash
ssh -i "secre.pem" ec2-user@ec2-3-222-101-80.compute-1.amazonaws.com
sudo su
docker pull registry.gitlab.com/afnarqui/login-aws-react-dynamodb-nodejs
docker run -dit -p 80:80 registry.gitlab.com/afnarqui/login-aws-react-dynamodb-nodejs
http://3.222.101.80:3002
````

[afn]: <https://gitlab.com/afnarqui/login-aws-react-dynamodb-nodejs>
